import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/service/game/game.service';
import { PlayerService } from 'src/app/service/player/player.service';

@Component({
  selector: 'app-app-game-component',
  templateUrl: './app-game-component.component.html',
  styleUrls: ['./app-game-component.component.scss']
})
export class AppGameComponentComponent implements OnInit {

  constructor(
    private readonly gameService: GameService,
    private readonly playerService: PlayerService) { 
      
    }

  ngOnInit(): void {
  }

}
