import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppGameComponentComponent } from './app-game-component.component';

describe('AppGameComponentComponent', () => {
  let component: AppGameComponentComponent;
  let fixture: ComponentFixture<AppGameComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppGameComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppGameComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
