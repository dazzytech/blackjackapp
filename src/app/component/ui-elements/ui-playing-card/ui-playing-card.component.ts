import { Component, OnInit } from '@angular/core';

/*
 * Visual design for a playing card
 */

@Component({
  selector: 'app-ui-playing-card',
  templateUrl: './ui-playing-card.component.html',
  styleUrls: ['./ui-playing-card.component.scss']
})
export class UiPlayingCardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
