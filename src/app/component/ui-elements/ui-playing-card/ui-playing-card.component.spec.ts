import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiPlayingCardComponent } from './ui-playing-card.component';

describe('UiPlayingCardComponent', () => {
  let component: UiPlayingCardComponent;
  let fixture: ComponentFixture<UiPlayingCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UiPlayingCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UiPlayingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
