import { Inject, Injectable } from '@angular/core';
import { CardService } from '../cards/card.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Card } from 'src/app/model/card.model';

/*
 * (Players turn)
 * Deals cards to player 
 * Deals cards to self until hard 17
 * store results in session
 * */

@Injectable({
  providedIn: 'root'
})
export class HouseService {

  constructor(
    private readonly cardsService: CardService,
    @Inject(SESSION_STORAGE) private readonly storage: WebStorageService
  ) { }

  // two cards to player
  dealHand() {
    this.storage.set('playerDeck', new Array(
      {
        items: [
          this.cardsService.takeOneCard(),
          this.cardsService.takeOneCard()]
      })
    );
  }

  hit(): Card {
    const newCard = this.cardsService.takeOneCard();

    if(newCard) {
      const deckName = this.storage.get('playerTurn') ? 'playerDeck' : 'houseDeck'
      
      const deck = this.storage.get(deckName);
      deck.push(newCard);
      this.storage.set(deckName, deck);

      return newCard;
    }
  
    throw("Error >> undefined card drawn");
  }

  stay() { 

  }

  // Proceed to draw cards in house till 17
  private dealHouse() {

  }
}
