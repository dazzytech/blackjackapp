import { Inject, Injectable } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

/*
 * Roles:
 * Update player games won and lost
 */

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private playerWins = 0;
  private playerLosses = 0;

  public get getWins(): number { return this.playerWins; }
  public get getLosses(): number { return this.playerLosses; }

  constructor(@Inject(SESSION_STORAGE) private readonly storage: WebStorageService) { }

  addWin() {
    this.playerWins++;
  }

  addLoss() {
    this.playerLosses++;
  }
}
