import { TestBed } from '@angular/core/testing';

import { PlayerService } from './player.service';

describe('AppPlayerServiceService', () => {
  let service: PlayerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should increment player wins', () => {
    service.addWin();
    expect(service.getWins).toEqual(1);
  })

  it('should increment player losses', () => {
    service.addLoss();
    expect(service.getLosses).toEqual(1);
  })
});
