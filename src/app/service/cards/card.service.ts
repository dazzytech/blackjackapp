import { Injectable } from '@angular/core';
import { SUIT, Card } from 'src/app/model/card.model';

/* 
 * Roles:
 * Create a set of 104 cards 2 * 52
 * randomize the location of the cards in the current deck
 * retrieve a random card
 * */
@Injectable({
  providedIn: 'root'
})
export class CardService {

  private cardRoster: Array<Card>;

  private currentDeck: Array<Card>;

  public get getCardRoster(): Card[] { return this.cardRoster; }

  public get getCurrentDeck(): Card[] { return this.currentDeck; }

  constructor() { 
    this.cardRoster = new Array();
    this.currentDeck = new Array();
  }

  generateRoster() {
    this.cardRoster = new Array();

    for(let i = 0; i < 8; i++) {
      this.generateSuit(i % 4, i * 13);
    }
  }

  generateSuit(suit: SUIT, startIndex: number) {
    this.cardRoster.push({ 
      id: startIndex,
      cardSuit: suit, 
      hardValue: 1,
      softValue: 11
    });

    for(let i = 2; i <= 13; i++) {
      this.cardRoster.push({ 
        id: startIndex + (i - 1),
        cardSuit: suit, 
        hardValue: i,
        softValue: i
      });
    }
  }

  shuffleDeck() {
    const MAX_CARDS = 104;

    this.currentDeck = this.cardRoster
      .map((a) => ({sort: Math.random(), value: a}))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value);
  }

  takeOneCard() {
    return this.currentDeck.pop();
  }
}
