import { ArrayType } from '@angular/compiler';
import { TestBed } from '@angular/core/testing';
import { Card } from 'src/app/model/card.model';

import { CardService } from './card.service';

describe('CardService', () => {
  let service: CardService;

  const MAX_CARDS = 104;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardService);

    service.generateRoster();
    service.shuffleDeck();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should generate the card roster of 104 unique cards', () => {
    expect(service.getCardRoster.length).toEqual(MAX_CARDS);
    expect(service.getCardRoster[0]).not.toEqual(service.getCardRoster[52]);
  })

  describe('Current Deck', () => {
    it('should shuffle the roster into a random order deck', () => {
      expect(service.getCurrentDeck).not.toEqual(service.getCardRoster);
    });

    it('should pull a unique card from the deck when takeOneCard() is called', () => {
      const cardDeck: Array<Card> = new Array();
      let duplicates = false;
      for(let i = 0; i < MAX_CARDS; i++) {
        const randomCard = service.takeOneCard();
        if(randomCard ) {
          if(cardDeck.includes(randomCard, 0)) {
            duplicates = true;
            break;
          }
          cardDeck.push(randomCard);
        }
      }

      expect(duplicates).toBeFalse();
      expect(cardDeck.length).toEqual(MAX_CARDS);
    });
  });
});
