import { Card } from "./card.model";

export class Session {
    playerDeck: Array<Card> | undefined;
    houseDeck: Array<Card> | undefined;
    playerTurn: Boolean = true;
}