export enum SUIT {
    HEARTS,
    SPADES,
    DIAMONDS,
    CLUBS
}

export class Card {
    public id: number = -1;
    public cardSuit: SUIT | undefined;
    public hardValue = 0;
    public softValue = 0;
}